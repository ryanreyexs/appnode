const express = require ('express');
const path= require('path');
const morgan =require('morgan');
const mysql= require('mysql');
const myConnection= require('express-myconnection');
const app = express();
//import routes
const estudianteroutes=require('./routes/estudiante');
const { urlencoded } = require('express');
//mysql
var mysqlhost  = process.env.MYSQL_HOST  || '192.168.175.135';
var mysqlport  = process.env.MYSQL_PORT  || '3306';
var mysqluser  = process.env.MYSQL_USER  || 'root';
var mysqlupass = process.env.MYSQL_PASS  || 'root';
var mysqldb    = process.env.MYSQL_DB    || 'sistemas';

//settings
app.set('port', process.env.PORT||3000);
app.set('view engine','ejs');
app.set('views', path.join(__dirname, 'views'));
//middleware
app.use(morgan('dev'));
app.use(myConnection(mysql,{
    host: mysqlhost,
    port: mysqlport,
    user: mysqluser,
    password: mysqlupass,
    database: mysqldb

},'single'));
app.use(express.urlencoded({extended:false}));
//rutas
app.use('/',estudianteroutes);
//statics files
app.use(express.static(path.join(__dirname, 'public')));

app.listen(app.get('port'),()=>{
    console.log("server on port 3000");
})