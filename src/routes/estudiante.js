const express=require('express');
const estudiantecontrolle=require('../controllers/estudiantecontroller');
const router= express.Router();
router.get('/', estudiantecontrolle.list);
router.post('/add',estudiantecontrolle.save)
router.get('/delete',estudiantecontrolle.delete)

module.exports= router;