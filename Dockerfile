FROM debian:latest
RUN apt-get update -y
RUN apt-get install nodejs npm -y
COPY src/ /app/
WORKDIR /app
EXPOSE 8005
CMD npm run dev
